FROM golang:1.13

RUN mkdir -p /app

WORKDIR /app

ADD . /app

RUN go get "github.com/tidwall/gjson"
RUN go build ./list_generator.go

CMD ["./list_generator"]

