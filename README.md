# ossci



# Introduction

This repo was created in order to generate a daily updated list of italian IPv4 addresses in various formats such as CSV, JSON and RAW

CSV:
```
"1.1.1.1/8","2.2.2.2/22","3.3.3.3/24"
```

JSON:
```
[
                "1.1.1.1/8",
                "2.2.2.2/22",
                "3.3.3.3/24
]
```

RAW:
```
1.1.1.1/8
2.2.2.2/22
3.3.3.3/24
```

All the data is stored automatically on the folder dataset/ everytime the CICD pipeline runs and collects the data from the input sources

The stored data at the moment is gathered from RIPE APIs and parsed into the expected formats