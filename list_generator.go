
package main

import (
   // "bufio"
    "fmt"
    "io/ioutil"
    "os"
    "io"
    "log"
    "time"
    "net/http"

   
    "github.com/tidwall/gjson"
)


func check(e error) {
    if e != nil {
        panic(e)
    }
}
var country string
var bodyString string
// Load contry IPv4 dataset and store it on multiple formats in order to reuse for any scanning tool e.g. nmap, masscan, zmap
// Usage


// Input JSON
// Output JSON, CSV, Raw IP list
func main() {

    // Get RIPE country from ENV vars, default country is IT
    if os.Getenv("COUNTRY") != ""  {
        country = os.Getenv("COUNTRY")
    } else {
        country = "IT" 
    }
    

    // Get yesterday date because ripestats are updated daily
    dt := time.Now().AddDate(0, 0, -1)
    //Format YYYY-MM-DD
    yesterday := dt.Format("2006-01-02")
  //  fmt.Println("Yesterday was ",yesterday)

    resp, err := http.Get("https://stat.ripe.net/data/country-resource-list/data.json?resource="+country+"&time="+yesterday)
    if err != nil {
        panic(err)
    }
    defer resp.Body.Close()

    out, err := os.Create("dataset/ripe-raw/data-"+country+"-"+yesterday+".json")
    if err != nil {
      // panic?
    }
    defer out.Close()
    io.Copy(out, resp.Body)

    fmt.Println("Response status:", resp.Status)

    // Reading stored raw json file
    content, err := ioutil.ReadFile("dataset/ripe-raw/data-"+country+"-"+yesterday+".json")
    if err != nil {
        log.Fatal(err)
    }

    // Convert []byte to string and print to screen
   // text := string(content)
  //  fmt.Println(text)

// data -- resources -- ipv4
    //fmt.Println("json",jsonparser.Get([]byte(bodyString), "company"))
   
// You can use `ArrayEach` helper to iterate items [item1, item2 .... itemN]
//jsonparser.ArrayEach([]byte(bodyString), func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
	//fmt.Println(jsonparser.Get(value, "url"))
//}, "data", "resources","ipv4")
  
// Extracting all country networks JSON RAW
value := gjson.Get(string(content), "data.resources.ipv4")
//fmt.Println("Extracted networks",value.String())

// Saving JSON RAW ip-IT-2020-07-24.json
f, err := os.Create("dataset/json/ip-"+country+"-"+yesterday+".json")
check(err)
defer f.Close()
_, err = f.WriteString(value.String()+"\n")
check(err)
f.Sync()


//gjson.ForEachLine(string(content), func(line gjson.Result) bool{
//    println(line.String())
//    return true
//})


// Saving IP RAW ip-IT-2020-07-24.raw
f2, err := os.Create("dataset/raw/ip-"+country+"-"+yesterday+".raw")
check(err)
defer f2.Close()

// Saving CSV list ip-IT-2020-07-24.csv
f3, err := os.Create("dataset/csv/ip-"+country+"-"+yesterday+".csv")
check(err)
defer f2.Close()

// For each country subnet gathered from RIPE
result := gjson.Get(string(content), "data.resources.ipv4")

for _, ipaddress := range result.Array() {
    //fmt.Println(ipaddress.String())
    _, err = f2.WriteString(ipaddress.String()+"\n")
    check(err)
    f2.Sync()
    //saving csv
    _, err = f3.WriteString(`"` +ipaddress.String()+`",`)
    check(err)
    f2.Sync()
}

}
